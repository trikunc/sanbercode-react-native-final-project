import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {LoginImage} from '../../assets';
import {Button, Input} from '../../components';
import {setForm} from '../../redux';
import {colors} from '../../utils';

const Login = ({navigation}) => {
  const {form} = useSelector((state) => state.loginReducer);

  const dispatch = useDispatch();

  const onInputChange = (value, InputForm) => {
    dispatch(setForm(value, InputForm));
  };

  const handleGoTo = (screen) => {
    navigation.navigate(screen);
  };

  return (
    <View style={styles.wrapper.page}>
      {/* <Button type="icon" name="back" onPress={() => navigation.goBack()} /> */}
      <ScrollView
        style={{
          flexDirection: 'column',
          alignSelf: 'center',
        }}>
        <LoginImage width={240} height={124} style={styles.illustration} />
        <Text style={styles.text.desc}>
          Mohon mengisi email dan password secara benar
        </Text>
        <View style={styles.space(30)} />
        <Input
          placeholder="Email"
          value={form.email}
          onChangeText={(value) => onInputChange(value, 'email')}
        />
        <View style={styles.space(5)} />
        <Input
          placeholder="Password"
          value={form.password}
          onChangeText={(value) => onInputChange(value, 'password')}
          secureTextEntry={true}
        />
        <View style={styles.space(40)} />
        <View
          style={{
            alignSelf: 'center',
            minWidth: 225,
          }}>
          <Button title="Login" onPress={() => handleGoTo('Home')} />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = {
  wrapper: {
    page: {padding: 20, flexDirection: 'column'},
  },
  illustration: {
    marginTop: 12,
  },
  text: {
    desc: {
      fontSize: 18,
      fontWeight: 'bold',
      color: colors.default,
      marginTop: 30,
      maxWidth: 250,
      textAlign: 'center',
    },
  },
  space: (value) => {
    return {height: value};
  },
};

export default Login;
