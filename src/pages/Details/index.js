import React, {useEffect} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {Button} from '../../components';
import {colors} from '../../utils';
navigator.geolocation = require('@react-native-community/geolocation');

const Home = ({navigation}) => {
  const {location, data, isLogin} = useSelector((state) => state.homeReducer);

  const celcius = (data) => Math.round(((data - 32) * 5) / 9);

  const handleGoTo = (screen) => {
    navigation.navigate(screen);
  };

  return (
    <View style={styles.wrapper.page}>
      <Button type="icon" name="back" onPress={() => navigation.goBack()} />
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
        }}>
        <Text style={styles.text.title}>DETAIL</Text>
        <Text style={styles.text.timezone}>Timezone: {data.timezone}</Text>
        <Text style={styles.text.latLong}>Latitude: {location.latitude}</Text>
        <Text style={styles.text.latLong}>Longitude: {location.longitude}</Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              alignItems: 'center',
              margin: 1,
              width: 172,
            }}>
            <View style={styles.space(20)} />
            <Text style={styles.text.day}>Today</Text>
            <View style={styles.space(10)} />
            {/* <Text style={styles.text.desc}>{data.iconNow}</Text> */}
            <View style={styles.space(10)} />
            <Text style={styles.text.icon}>{data.iconNow}</Text>
            <View style={styles.space(10)} />
            <View style={{height: 90}}>
              <Text style={styles.text.desc}>{data.summaryNow}</Text>
            </View>
            <View style={styles.space(10)} />
            <Text style={styles.text.temp}>
              {Math.round(data.temperatureNow)} F /{' '}
              {celcius(data.temperatureNow)} C
            </Text>
          </View>
          <View></View>
          <View
            style={{
              alignItems: 'center',
              margin: 1,
              width: 172,
            }}>
            <View style={styles.space(20)} />
            <Text style={styles.text.day}>Tomorrow</Text>
            <View style={styles.space(10)} />
            {/* <Text style={styles.text.desc}>{data.summaryNow}</Text> */}
            <View style={styles.space(10)} />
            <Text style={styles.text.icon}>{data.iconTomorrow}</Text>
            <View style={styles.space(10)} />
            <View style={{height: 90}}>
              <Text style={styles.text.desc}>{data.summaryTomorrow}</Text>
            </View>

            <View style={styles.space(10)} />
            <Text style={styles.text.temp}>
              {Math.round(data.tempHighTomorrow)} F /{' '}
              {celcius(data.tempHighTomorrow)} C
            </Text>
          </View>
        </View>
        <View
          style={{
            alignSelf: 'center',
            minWidth: 225,
            marginTop: 20,
          }}>
          <Button title="About" onPress={() => handleGoTo('About')} />
        </View>
      </View>
    </View>
  );
};

const styles = {
  wrapper: {
    page: {padding: 10, flexDirection: 'column'},
  },
  illustration: {
    marginTop: 12,
  },

  text: {
    timezone: {
      fontSize: 18,
      fontWeight: 'bold',
      color: colors.default,
      marginTop: 20,
    },
    latLong: {
      fontSize: 16,
      color: colors.default,
    },

    day: {
      fontSize: 16,
      color: colors.default,
    },
    icon: {
      fontSize: 17,
      fontWeight: 'bold',
      color: colors.default,
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
      color: colors.default,
      marginTop: 30,
      maxWidth: 250,
      textAlign: 'center',
    },
    desc: {
      fontSize: 16,
      fontWeight: 'bold',
      color: colors.default,
      marginTop: 30,
      maxWidth: 250,
      textAlign: 'center',
    },
    temp: {
      fontSize: 22,
      fontWeight: 'bold',
      color: colors.default,
    },
  },
  space: (value) => {
    return {height: value};
  },
};

export default Home;
