import React, {useEffect} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {setLocation, setData} from '../../redux';
import axios from 'axios';
import {Button} from '../../components';
import {colors} from '../../utils';
import Skycons from 'react-skycons';
navigator.geolocation = require('@react-native-community/geolocation');

const API_KEY = '0e2d9086db780170d168d93e0b95362a';

const Home = ({navigation}) => {
  const {location, data, isLogin} = useSelector((state) => state.homeReducer);
  const dispatch = useDispatch();

  const sendData = () => {
    console.log('data yg dikirim: ');
  };

  useEffect(() => {
    const fetchData = () => {
      return navigator.geolocation.getCurrentPosition((position) => {
        // console.log(position);
        let lat = position.coords.latitude;
        let long = position.coords.longitude;
        onInputChange(lat, long);

        fetchWeather(lat, long);
        console.log(lat, long);
      });
    };
    fetchData();
  }, []);

  fetchWeather = async (lat, lon) => {
    const api_call = axios.get(
      `https://api.darksky.net/forecast/653d34e99e9f1ea8985cf6cf98ed49f0/${lat},${lon}`,
    );
    const {data} = await api_call;

    const timezone = data.timezone;
    const temperatureNow = data.currently.temperature;
    const timeNow = data.currently.time;

    const iconsNow = data.currently.icon;
    const iconNow = iconsNow.toUpperCase().replace(/-/g, '_');

    const summaryNow = data.currently.summary;
    const windNow = data.currently.windSpeed;
    const tempHighNow = data.daily.data[0].temperatureHigh;
    const tempLowNow = data.daily.data[0].temperatureLow;

    const iconsTomorrow = data.daily.data[1].icon;
    const iconTomorrow = iconsTomorrow.toUpperCase().replace(/-/g, '_');

    const summaryTomorrow = data.daily.data[1].summary;
    const windTomorrow = data.daily.data[1].windSpeed;
    const tempHighTomorrow = data.daily.data[1].temperatureHigh;
    const tempLowTomorrow = data.daily.data[1].temperatureLow;

    dataWeatherHandler(
      timezone,
      temperatureNow,
      timeNow,
      iconNow,
      summaryNow,
      windNow,
      tempHighNow,
      tempLowNow,
      iconTomorrow,
      summaryTomorrow,
      windTomorrow,
      tempHighTomorrow,
      tempLowTomorrow,
    );
  };

  const onInputChange = (lat, long) => {
    dispatch(setLocation(lat, long));
    // console.log(lat);
  };

  const dataWeatherHandler = (
    timezone,
    temperatureNow,
    timeNow,
    iconNow,
    summaryNow,
    windNow,
    tempHighNow,
    tempLowNow,
    iconTomorrow,
    summaryTomorrow,
    windTomorrow,
    tempHighTomorrow,
    tempLowTomorrow,
  ) => {
    dispatch(
      setData(
        timezone,
        temperatureNow,
        timeNow,
        iconNow,
        summaryNow,
        windNow,
        tempHighNow,
        tempLowNow,
        iconTomorrow,
        summaryTomorrow,
        windTomorrow,
        tempHighTomorrow,
        tempLowTomorrow,
      ),
    );
  };

  const handleGoTo = (screen) => {
    navigation.navigate(screen);
  };

  const celcius = (data) => Math.round(((data - 32) * 5) / 9);

  return (
    <View style={styles.wrapper.page}>
      <Button type="icon" name="back" onPress={() => navigation.goBack()} />
      <ScrollView
        style={{
          flexDirection: 'column',
          alignSelf: 'center',
        }}>
        <Text style={styles.text.title}>HOME</Text>
        {/* <SetIcon icon={data.icon} /> */}
        {/* <Skycons color="red" icon="PARTLY_CLOUDY_DAY" autoplay={false} /> */}
        <Text style={styles.text.desc}>{data.timezone}</Text>
        <View style={styles.space(20)} />
        <Text style={styles.text.desc}>{data.iconNow}</Text>
        <View style={styles.space(10)} />
        <Text style={styles.text.desc}>{data.summaryNow}</Text>
        <View style={styles.space(10)} />
        <Text style={styles.text.desc}>
          {Math.round(data.temperatureNow)} F / {celcius(data.temperatureNow)} C
        </Text>
        <View style={styles.space(40)} />

        <View
          style={{
            alignSelf: 'center',
            minWidth: 225,
          }}>
          <Button title="Detail" onPress={() => handleGoTo('Details')} />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = {
  wrapper: {
    page: {padding: 20, flexDirection: 'column'},
  },
  illustration: {
    marginTop: 12,
  },
  text: {
    title: {
      fontSize: 20,
      fontWeight: 'bold',
      color: colors.default,
      marginTop: 30,
      maxWidth: 250,
      textAlign: 'center',
    },
    desc: {
      fontSize: 24,
      fontWeight: 'bold',
      color: colors.default,
      marginTop: 30,
      maxWidth: 250,
      textAlign: 'center',
    },
  },
  space: (value) => {
    return {height: value};
  },
};

export default Home;
