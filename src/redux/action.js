export const setForm = (value, InputForm) => {
  return {
    type: 'SET_FORM',
    inputValue: value,
    inputForm: InputForm,
  };
};

export const setLocation = (lat, long) => {
  return {
    type: 'SET_LOCATION',
    inputLat: lat,
    inputLong: long,
  };
};

export const setData = (
  timezone,
  temperatureNow,
  timeNow,
  iconNow,
  summaryNow,
  windNow,
  tempHighNow,
  tempLowNow,
  iconTomorrow,
  summaryTomorrow,
  windTomorrow,
  tempHighTomorrow,
  tempLowTomorrow,
) => {
  return {
    type: 'SET_DATA',
    Timezone: timezone,
    TemperatureNow: temperatureNow,
    TimeNow: timeNow,
    IconNow: iconNow,
    SummaryNow: summaryNow,
    WindNow: windNow,
    TempHighNow: tempHighNow,
    TempLowNow: tempLowNow,
    IconTomorrow: iconTomorrow,
    SummaryTomorrow: summaryTomorrow,
    WindTomorrow: windTomorrow,
    TempHighTomorrow: tempHighTomorrow,
    TempLowTomorrow: tempLowTomorrow,
  };
};
