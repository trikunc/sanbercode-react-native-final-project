import {combineReducers} from 'redux';

// const initialStateRegister = {
//   form: {
//     fullName: '',
//     email: '',
//     password: '',
//   },
//   title: 'Register Page BRO',
//   desc: 'Ini adalah desc intuk Register',
// };

const initialStateLogin = {
  form: {
    email: '',
    password: '',
  },
  info: 'Tolong masukan password anda',
  isLogin: true,
};

const initialStateHome = {
  location: {
    latitude: '',
    longitude: '',
  },
  data: {
    timezone: '',
    temperatureNow: '',
    timeNow: '',
    iconNow: '',
    summaryNow: '',
    windNow: '',
    tempHighNow: '',
    tempLowNow: '',
    iconTomorrow: '',
    summaryTomorrow: '',
    windTomorrow: '',
    tempHighTomorrow: '',
    tempLowTomorrow: '',
  },
  isLogin: true,
};

// const registerReducer = (state = initialStateRegister, action) => {
//   if (action.type === 'SET_FORM') {
//     return {
//       ...state,
//       form: {
//         ...state.form,
//         [action.inputForm]: action.inputValue,
//       },
//     };
//   }
//   return state;
// };

const loginReducer = (state = initialStateLogin, action) => {
  if (action.type === 'SET_FORM') {
    return {
      ...state,
      form: {
        ...state.form,
        [action.inputForm]: action.inputValue,
      },
    };
  }
  return state;
};

const homeReducer = (state = initialStateHome, action) => {
  if (action.type === 'SET_LOCATION') {
    return {
      ...state,
      location: {
        ...state.location,
        latitude: action.inputLat,
        longitude: action.inputLong,
      },
    };
  }

  if (action.type === 'SET_DATA') {
    return {
      ...state,
      data: {
        ...state.data,
        timezone: action.Timezone,
        temperatureNow: action.TemperatureNow,
        timeNow: action.TimeNow,
        iconNow: action.IconNow,
        summaryNow: action.SummaryNow,
        windNow: action.WindNow,
        tempHighNow: action.TempHighNow,
        tempLowNow: action.TempLowNow,
        iconTomorrow: action.IconTomorrow,
        summaryTomorrow: action.SummaryTomorrow,
        windTomorrow: action.WindTomorrow,
        tempHighTomorrow: action.TempHighTomorrow,
        tempLowTomorrow: action.TempLowTomorrow,
      },
    };
  }

  return state;
};

const reducer = combineReducers({
  homeReducer,
  loginReducer,
});

export default reducer;
